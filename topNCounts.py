import heapq
import json
from collections import UserDict

# modify to the file directory you want
filePrefix = 'C:\\Users\\95301\\Downloads\\'

class topKTweetsInStream:
    def __init__(self, tweetsSource, concernType, k):
        self.filename = tweetsSource
        self.type = concernType
        self.k = k

    def topK(self):
        with open(filePrefix+self.filename) as f:
            tweets = (json.loads(obj) for obj in json.load(f)['data'])

            # time complexity O(n*log(n))
            topKList = heapq.nlargest(self.k, tweets, key=lambda x:x['user'][self.type])

            # time complexity O(n)
            #topKList = []
            #heapq.heapify(topKList)
            #for item in tweets:
            #    if len(topKList)<self.k:
            #        heapq.heappush(topKList, item)
            #    else:
            #        if topKList[0][self.type]<item[self.type]:
            #            heapq.heapreplace(topKList, item)
            return json.dumps({'topK':topKList})


def main():
    jsonFiles = ['netflix.json', 'google.json']

    for file in jsonFiles:
        top10_quoted = topKTweetsInStream(file, 'followers_count', 10)
        top10_quotedlist = top10_quoted.topK()
        print("top10 quoted tweets list")
        for tweet in json.loads(top10_quotedlist)['topK']:
            print(tweet['id'], tweet['user']['followers_count'])

        top10_reply = topKTweetsInStream(file, 'friends_count', 10)
        top10_replylist = top10_reply.topK()
        print("top10 replied tweets list")
        for tweet in json.loads(top10_replylist)['topK']:
            print(tweet['id'], tweet['user']['friends_count'])

        



if __name__=="__main__":
    main()

            
            

